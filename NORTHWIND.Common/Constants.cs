﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NORTHWIND.Common
{
    public class Constants
    { 
        public const string DATE_FORMAT_STRING = "dd-MMM-yyyy";
        public const string DATETIME_FORMAT_STRING = "dd-MMM-yyyy HH:mm:ss";
        public const string DATETIME_FORMAT_STRING_SQL = "yyyy-mm-dd HH:mm:ss";
        public const string TIMEHOURS_FORMAT_STRING = "HH";
        public const string TIMEMINUTES_FORMAT_STRING = "mm";

        public const string REMOTE_ADDRESS = "REMOTE_ADDR";

        //Session 
        public static string USER_ID = "USER_ID";
        public const string CURRENT_USER = "CURRENT_USER";
        public const string CURRENT_USER_NAME = "CURRENT_USER_NAME";
        public const string SESSION_ERROR = "SESSION_ERROR"; 
        public const string SIMPLE_LOGIN_USER = "SIMPLE_LOGIN_USER";
        public const string MENU_SESSION = "MENU_SESSION";
        public const string USER_PASSWORD = "USER_PASSWORD";
        public const string USER_ROLENAME = "USER_ROLENAME";
        public const string USER_ROLE = "USER_ROLE";
        public const string LOGIN_ID = "LOGIN_ID";
        public const string DOMAIN_NAME = "DOMAIN_NAME";
        public const string LOGIN_LOG = "LOGIN_LOG";

        //Status
        public const string STATUS_INPROGRESS = "IN PROGRESS";
        public const string STATUS_COMPLETED = "COMPLETE";
        public const string STATUS_CREATED = "CREATED";
        public const string STATUS_APPROVED = "APPROVED";
        public const string STATUS_REJECTED = "REJECTED";
        public const string STATUS_REVISED = "REVISED";
        public static int STATUS_ACTIVE_INCIDENT = 1;
        public static int STATUS_REVISED_INCIDENT = 2;
        public static int STATUS_COMPLETED_INCIDENT = 3;
        public static int STATUS_REJECTED_INCIDENT = 5;

        public const int ACTIVE_USER = 1;
        public const int NOT_ACTIVE_USER = 2;
    }
}
