﻿using NORTHWIND.Common;
using NORTHWIND.Entity;
using NORTHWIND.Facade;
using NORTHWIND.WebMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NORTHWIND.WebMVC.Controllers
{
    public class CustomersController : Controller
    {
        // GET: Customers
        public ActionResult Index()
        { 
            return View();
        }


        [Route("List")]
        public ActionResult List()
        {
            var facade = new CustomersFacade();
            return Json(ApiResponse.OK(facade.GetAllItems()),JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Route("Paging")]
        public ActionResult Paging(PageData data)
        {
            var facade = new CustomersFacade();
            Dictionary<string, object> param = new Dictionary<string, object>();
            if (data.criteria != null)
                foreach (var crt in data.criteria)
                {
                    param[crt.criteria] = crt.value;
                }
            var rows = facade.GetListItemsByLikeCriteria(param, data.page, data.pageSize, data.order);
            var count = facade.GetListItemsByLikeCriteriaCount(param);
            var pageCount = Math.Ceiling(Commons.GetDouble(count) / data.pageSize);
            var apiResponse = ApiResponse.OK(new { rows = rows, rowCount = count, pageCount = pageCount });
            return Json(apiResponse);
        }

        [HttpPost]
        public ActionResult Create()
        {
            return this.PartialView();
        }

        // GET: Customers/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateSave(CustomerView model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.PartialView("Create", model);
            }

            // todo db
            var facade = new CustomersFacade();
            var customer = new Customers();
            customer.CustomerID = model.CustomerID;
            customer.CompanyName = model.CompanyName;
            customer.ContactName = model.ContactName;
            customer.ContactTitle = model.ContactTitle;
            customer.Address = model.Address;
            customer.City = model.City;
            customer.Region = model.Region;
            customer.PostalCode = model.PostalCode;
            customer.Country = model.Country;
            customer.Phone = model.Phone;
            customer.Fax = model.Fax;

            try
            {
                facade.Insert(customer);
                return (ActionResult)this.Content(@"ok");
            }
            catch (Exception ex)
            {
                this.ModelState.AddModelError(String.Empty, ex.Message);
            }
            return this.PartialView("Create", model);
        }

        [HttpPost]
        public ActionResult AddNewCustomer()
        {


            return View("Index");
        }

        // POST: Customers/Create
        //[HttpPost]
        //public ActionResult Create(FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add insert logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        // GET: Customers/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Customers/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Customers/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Customers/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
