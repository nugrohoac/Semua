﻿using NORTHWIND.Common;
using NORTHWIND.Entity;
using NORTHWIND.Facade;
using NORTHWIND.WebMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NORTHWIND.WebMVC.Controllers
{
    public class SuppliersController : Controller
    {
        // GET: Suppliers
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("Paging")]
        public ActionResult Paging( PageData data)
        {
            var facade = new SupplierFacade();
            Dictionary<string, object> param = new Dictionary<string, object>();
            if (data.criteria != null)
                foreach (var crt in data.criteria)
                {
                    param[crt.criteria] = crt.value;
                }
            //query
            var rows = facade.GetListItemsByLikeCriteria(param, data.page, data.pageSize, data.order);
            var count = facade.GetListItemsByLikeCriteriaCount(param);
            var pageCount = Math.Ceiling(Commons.GetDouble(count) / data.pageSize);
            var apiResponse = ApiResponse.OK(new { rows = rows, rowCount = count, pageCount = pageCount });
            return Json(apiResponse);
        }

        // GET: Suppliers/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create()
        {
            return this.PartialView();
        }

        [HttpPost]
        public ActionResult CreateSave(SupplierView model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.PartialView("Create", model);
            }

            // todo db
            var facade = new SupplierFacade();
            var supplier = new Suppliers();
            //supplier.SupplierID = model.SupplierID;
            supplier.CompanyName = model.CompanyName;
            supplier.ContactName = model.ContactName;
            supplier.ContactTitle = model.ContactTitle;
            supplier.Address = model.Address;
            supplier.City = model.City;
            supplier.Region = model.Region;
            supplier.PostalCode = model.PostalCode;
            supplier.Country = model.Country;
            supplier.Phone = model.Phone;
            supplier.Fax = model.Fax;
            supplier.HomePage = model.HomePage;

            try
            {
                facade.Insert(supplier);
                return (ActionResult)this.Content(@"ok");
            }
            catch (Exception ex)
            {
                this.ModelState.AddModelError(String.Empty, ex.Message);
            }
            return this.PartialView("Create", model);
        }


        //// POST: Suppliers/Create
        //[HttpPost]
        //public ActionResult Create(FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add insert logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        // GET: Suppliers/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Suppliers/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Suppliers/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Suppliers/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
