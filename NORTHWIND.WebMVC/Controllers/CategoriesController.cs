﻿using NORTHWIND.Common;
using NORTHWIND.Entity;
using NORTHWIND.Facade;
using NORTHWIND.WebMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NORTHWIND.WebMVC.Controllers
{
    public class CategoriesController : Controller
    {
        // GET: Categories
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("Paging")]
        public ActionResult Paging(PageData data)
        {
            var facade = new CategoriesFacade();
            Dictionary<string, object> param = new Dictionary<string, object>();
            if (data.criteria != null)
                foreach (var crt in data.criteria)
                {
                    param[crt.criteria] = crt.value;
                }
            //query
            var rows = facade.GetListItemsByLikeCriteria(param, data.page, data.pageSize, data.order);
            var list = new List<CategoryMap>();
            foreach(var item in rows)
            {
                var obj = new CategoryMap();
                obj.CategoryID = item.CategoryID;
                obj.CategoryName = item.CategoryName;
                obj.Description = item.Description;
                obj.Picture = item.Picture;
                int len = item.Picture.Length; 
                obj.StrImage = len > 78 ? Convert.ToBase64String(item.Picture, 78, len - 78):"" ;
                list.Add(obj);
            }
            var count = facade.GetListItemsByLikeCriteriaCount(param);
            var pageCount = Math.Ceiling(Commons.GetDouble(count) / data.pageSize);
            var apiResponse = ApiResponse.OK(new { rows = list, rowCount = count, pageCount = pageCount });
            return Json(apiResponse);
        }

        // GET: Categories/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Categories/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Categories/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Categories/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Categories/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Categories/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Categories/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
