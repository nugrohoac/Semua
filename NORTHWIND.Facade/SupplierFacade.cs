﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using NORTHWIND.Entity;
using PetaPoco;

namespace NORTHWIND.Facade
{
    public class SupplierFacade : BaseCRUD<Suppliers>
    {
        public Page PageDataByCriteria(string criteria, int page, int perPage, string orderBy)
        {
            string sql = @"SELECT * FROM [dbo].[Suppliers] WHERE " + criteria;
            return DB.Page(page, perPage, new Sql(sql).OrderBy(orderBy));
        }

        public new void Insert(Suppliers obj)
        {
            DB.Insert("Suppliers", "SupplierID", true, obj);
        }

        public new bool DataInsert(Suppliers cus, out string errorMsg)
        {
            errorMsg = "";
            try
            {
                using (var scope = DB.GetTransaction())
                {
                    Insert(cus);
                    scope.Complete();
                }
            }
            catch (Exception ex) { errorMsg = ex.Message.Replace("'", ""); return false; }
            return true;
        }


        public bool DataDelete(string id, out string errorMsg)
        {
            errorMsg = "";
            try
            {
                var cus = GetItemByID(id);
                if (cus == null) throw new Exception("Data not found!");
                using (var scope = DB.GetTransaction())
                {
                    DB.Delete<Customers>(new Sql().Where("SupplierID = @0", id));
                    scope.Complete();
                }
            }
            catch (Exception ex) { errorMsg = Regex.Replace(ex.Message.Replace("'", ""), @"\t|\n|\r", ""); return false; }
            return true;
        }
    }
}
