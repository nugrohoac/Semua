﻿using NORTHWIND.Entity;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NORTHWIND.Facade
{
    public class CategoriesFacade : BaseCRUD<Categories>
    {
        public new List<Categories> GetListItemsByCriteria(Dictionary<string, object> param, int page, int itemsPerPage, string orderBy)
        {
            string sql = " 1=1 ";
            object[] cv = new object[param.Count];
            int i = 0;
            foreach (string key in param.Keys)
            {
                sql += string.Format("AND {0} = @{1} ", key, i);
                cv[i] = param[key];
                i++;
            }
            var list = DB.Fetch<Categories>(new Sql().Where(sql, cv).OrderBy(orderBy));
            list = DB.Page<Categories>(page, itemsPerPage, new Sql().Where(sql, cv).OrderBy(orderBy)).Items;
            foreach(var item in list)
            {
                
            }
            return list;
        }
    }
}
