﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NORTHWIND.Entity
{
    [TableName("Shippers")]
    [PrimaryKey("ShipperID")]
    [ExplicitColumns]
    [Serializable]
    public class Shippers
    {
        [Column]
        public int ShipperID { get; set; }

        [Column]
        public string CompanyName { get; set; }

        [Column]
        public string Phone { get; set; }
    }
}
