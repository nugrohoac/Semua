﻿using PetaPoco;
using System; 

namespace NORTHWIND.Entity
{
    [TableName("Customers")]
    [PrimaryKey("CustomerID")]
    [ExplicitColumns]
    [Serializable]
    public class Customers
    {
        [PetaPoco.Column]
        public string CustomerID { get; set; }

        [PetaPoco.Column]
        public string CompanyName { get; set; }

        [PetaPoco.Column]
        public string ContactName { get; set; }

        [PetaPoco.Column]
        public string ContactTitle { get; set; }

        [PetaPoco.Column]
        public string Address { get; set; }

        [PetaPoco.Column]
        public string City { get; set; }

        [PetaPoco.Column]
        public string Region { get; set; }

        [PetaPoco.Column]
        public string PostalCode { get; set; }

        [PetaPoco.Column]
        public string Country { get; set; }

        [PetaPoco.Column]
        public string Phone { get; set; }

        [PetaPoco.Column]
        public string Fax { get; set; }
    }
}
